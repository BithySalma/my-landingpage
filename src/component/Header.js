import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { AppBar, Collapse, IconButton, Toolbar } from '@material-ui/core';
import CalendarViewDayIcon from '@material-ui/icons/CalendarViewDay';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Link as Scroll} from 'react-scroll';
const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        
        height: '100vh',
        fontFamily: 'Nunito',
      },
  appbar: {
      background:'none',
      fontFamily: 'Nunito',
  },
  appbarWrapper: {
    width: '80%',
    margin: '0 auto',
  },
  appbarTitle:{

    flexGrow: '1',
  },
  icon:{
      color: '#fff',
      fontSize: '2rem',
  },
  title:{
      color:'#fff',
      fontSize: '3rem',
      padding: '100px',
  },
   container: {
    textAlign: 'center',
   },
   goDown:{
       color: '#fff',
       fontSize: '5rem',
   },
}));

export default function Header() {
    const classes = useStyles();
    const [checked, setChecked] = useState(false);
    useEffect(() => {
      setChecked(true);
    }, []);
    return(
        <div className = {classes.root} id = "header">
       <AppBar className = {classes.appbar} elevation ={0}>
       <Toolbar className={classes.appbarWrapper}>
           <h1 className = {classes.appbarTitle}>MY LANDING PAGE</h1>
           <Scroll to="place-to-">
           <IconButton>
            <CalendarViewDayIcon className = {classes.icon}/>
           </IconButton>
           </Scroll>
           
           </Toolbar>
           
       </AppBar>

       <Collapse
        in={checked}
        {...(checked ? { timeout: 1000 } : {})}
        collapsedHeight={50}
      >
        <div className={classes.container}>
          <h1 className={classes.title}>
            Welcome to <br />
            My<span className={classes.colorText}>Island.</span>
          </h1>
          <Scroll to="place-to-visit" smooth={true}>
            <IconButton>
              <ExpandMoreIcon className={classes.goDown} />
            </IconButton>
          </Scroll>
        </div>
      </Collapse>
    </div>
  );
}